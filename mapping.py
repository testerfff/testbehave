# Created by visakov at 28.02.18

# -*- coding: utf-8 -*-


host_url = 'https://www.sipnet.ru'

site_mapping = {
    "authorization": {
        "url": host_url + "/cabinet-login",
        "url cabinet": host_url + "/cabinet/",
        "username": "//*[@id='login-field']",
        "password": "//*[@id='password-field']",
        "login button": "//*[@id='login-form-submit']",
        "status message": "//*[@class='error']",
        "profile": "//*[@class='user-panel-info']",
        "content": "//*[@id='content']",
    },
    "registration": {
        "url": "http://uralsib-site-dev.liteproject.ru/registration/",
        "btn_modal": "//button[@id='btnShowCityModal']",
        "i_login": "//input[@id='LOGIN']",
        "i_pass": "//input[@id='PASSWORD']",
        "i_pass_conf": "//input[@id='CONFIRM_PASSWORD']",
        "i_email": "//input[@id='EMAIL']",
        "i_name": "//input[@id='NAME']",
        "i_last_name": "//input[@id='LAST_NAME']",
        "i_second_name": "//input[@id='SECOND_NAME']",
        "lst_sex": "//div[@class='nice-select select__control wide']",
        "lst_sex_open": "//div[@class='nice-select select__control wide open']",
        "i_birthday": "//input[@id='PERSONAL_BIRTHDAY']",
        "i_phone": "//input[@id='PERSONAL_PHONE']",
        "lst_marital_status": "//div[@class='nice-select select__control wide open']",
        "i_count_child": "UF_COUNT_CHILDRENS",
        "i_birth_place": "UF_BIRTH_PLACE",

        "i_passport_series": "UF_PASSPORT_SERIES",
        "i_passport_number": "UF_PASSPORT_NUMBER",
        "i_passport_issuedby": "UF_PASSPORT_ISSUEDBY",
        "i_passport_date": "UF_PASS_DATE_ISSUE",
        "i_passport_dep_code": "UF_P_DEPARTMENT_CODE",

        "i_reg_address_reg": "UF_REG_ADDRES_REGION",
        "i_reg_address_index": "UF_REG_ADDRES_INDEX",
        "i_reg_address_city": "UF_REG_ADDRESS_CITY",
        "i_reg_address_street": "UF_REG_ADDRESS_STREA",
        "i_reg_address_house": "UF_REG_ADDRES_HOUSE",
        "i_reg_address_build": "UF_REG_ADDRES_BUILDI",
        "i_reg_address_bulk": "UF_REG_ADDRESS_BULK",
        "i_reg_address_apart": "UF_REG_ADDRESS_APART",

        "chk_address_equal": "//label[@for='UF_ADDRESS_ARE_EQAL']",

        "i_fact_address_region": "UF_FACT_ADDRES_REGIO",
        "i_fact_address_index": "UF_FACT_ADDRES_INDEX",
        "i_fact_address_city": "UF_FACT_ADDRESS_SITY",
        "i_fact_address_street": "UF_FACT_ADDRESS_STRE",
        "i_fact_address_house": "UF_FA_HOUSE",
        "i_fact_address_build": "UF_FA_BUILDING",
        "i_fact_address_bulk": "UF_FA_BULK",
        "i_fact_address_apart": "UF_FA_APARTMENT",

        "btn_submit": "register_submit_button",

        "error_text_hint": "//*[@class='input__hint-text']",
    },
    "forgot_password": {
        "url": "http://uralsib-site.liteproject.ru/auth/forgotpasswd/",
        "btn_modal": "//button[@id='btnShowCityModal']",
        "i_email_forgot": "USER_LOGIN",
        "btn_submit": "send_account_info",
        "error_text_hint": "//*[@class='input__hint']/div",
    },
    "change_password": {
        "url": "http://uralsib-site.liteproject.ru/auth/changepassword/",
        "btn_modal": "//button[@id='btnShowCityModal']",
        "i_login": "USER_LOGIN",
        "i_check_string": "USER_CHECKWORD",
        "i_new_password": "USER_PASSWORD",
        "i_new_password_confirm": "USER_CONFIRM_PASSWORD",
        "btn_submit": "//*[@name='change_pwd']",
        "error_text_hint_login": "//*[@class='input__hint']/div",
        "error_text_hint_password": "//*[@class='input__hint']/div",
        "error_text_hint_password_not_match": "//*[@class='input__hint']/div",
    },
    "confirm_email": {
        "url": "http://uralsib-site.liteproject.ru/registration/confirmation/",
        "btn_modal": "//button[@id='btnShowCityModal']",
        "i_login": "login",
        "i_code": "confirm_code",
        "btn_submit": "//*[@class='form-box__constrols row']//input",
        "error_text_hint": "//*[@class='card-promo__subtitle-card']",
    },
}
