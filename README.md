# README #

start project : **28.02.2018**


# Command #

#### Запуск конкретного сценария
* behave -f plain features/test_05.feature:38
* behave -n 'Confirmation Email "Not a valid user"'

#### Получение атрибута из context
* context.value = getattr(context, name, None)