<!--Created by visakov at 05.03.18-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" href="../../templates/style.css"/>
            </head>
            <body>
                <div class="toc-contents">
                    <xsl:apply-templates/>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="testsuite">

        <div class="toc-testsuite">
            <h1>
                <xsl:value-of select="@name"/>
            </h1>

            <table width="100%" border="1">
                <THEAD>
                    <TR>
                        <TD>
                            <B>
                                <xsl:value-of select="name(@tests)"/>
                            </B>
                        </TD>
                        <TD>
                            <B>
                                <xsl:value-of select="name(@errors)"/>
                            </B>
                        </TD>
                        <TD>
                            <B>
                                <xsl:value-of select="name(@failures)"/>
                            </B>
                        </TD>
                        <TD>
                            <B>
                                <xsl:value-of select="name(@skipped)"/>
                            </B>
                        </TD>
                        <TD>
                            <B>
                                <xsl:value-of select="name(@time)"/>
                            </B>
                        </TD>
                        <TD>
                            <B>
                                <xsl:value-of select="name(@timestamp)"/>
                            </B>
                        </TD>
                    </TR>
                </THEAD>
                <TBODY>
                    <TR>
                        <TD>
                            <xsl:value-of select="@tests"/>
                        </TD>
                        <TD>
                            <xsl:value-of select="@errors"/>
                        </TD>
                        <TD>
                            <xsl:value-of select="@failures"/>
                        </TD>
                        <TD>
                            <xsl:value-of select="@skipped"/>
                        </TD>
                        <TD>
                            <xsl:value-of select="@time"/>
                        </TD>
                        <TD>
                            <xsl:value-of select="@timestamp"/>
                        </TD>
                    </TR>
                </TBODY>
            </table>

            <!--<ul>-->
            <!--<li>-->
            <!--<xsl:value-of select="name(@tests)"/>-->
            <!--:-->
            <!--<xsl:value-of select="@tests"/>-->
            <!--</li>-->
            <!--<li>-->
            <!--<xsl:value-of select="name(@errors)"/>-->
            <!--:-->
            <!--<xsl:value-of select="@errors"/>-->
            <!--</li>-->
            <!--<li>-->
            <!--<xsl:value-of select="name(@failures)"/>-->
            <!--:-->
            <!--<xsl:value-of select="@failures"/>-->
            <!--</li>-->
            <!--<li>-->
            <!--<xsl:value-of select="name(@skipped)"/>-->
            <!--:-->
            <!--<xsl:value-of select="@skipped"/>-->
            <!--</li>-->
            <!--<li>-->
            <!--<xsl:value-of select="name(@time)"/>-->
            <!--:-->
            <!--<xsl:value-of select="@time"/>-->
            <!--</li>-->
            <!--<li>-->
            <!--<xsl:value-of select="name(@timestamp)"/>-->
            <!--:-->
            <!--<xsl:value-of select="@timestamp"/>-->
            <!--</li>-->
            <!--</ul>-->
        </div>

        <xsl:apply-templates select="testcase"/>

    </xsl:template>

    <xsl:template match="testcase">
        <xsl:choose>
            <xsl:when test="@status = 'passed'">
                <div class="toc-testcase" id="success">
                    <h2>
                        <xsl:value-of select="@name"/>
                    </h2>
                    <ul>
                        <li>
                            <xsl:value-of select="name(@status)"/>
                            :
                            <xsl:value-of select="@status"/>
                        </li>
                        <li>
                            <xsl:value-of select="name(@time)"/>
                            :
                            <xsl:value-of select="@time"/>
                        </li>
                    </ul>
                </div>
            </xsl:when>

            <xsl:otherwise>
                <div class="toc-testcase" id="error">
                    <h2>
                        <xsl:value-of select="@name"/>
                    </h2>
                    <ul>
                        <li>
                            <xsl:value-of select="name(@status)"/>
                            :
                            <xsl:value-of select="@status"/>
                        </li>
                        <li>
                            <xsl:value-of select="name(@time)"/>
                            :
                            <xsl:value-of select="@time"/>
                        </li>
                    </ul>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>