Feature: Authorization

Feature: Registration

Feature: Forgot Password

Feature: Change Password

Feature: Confirmation Email

  Scenario: Confirmation Email "Not a valid user"
    Given website open 'confirm_email' ... passed in 7.190s
    When click element on 'btn_modal' ... passed in 0.769s
    When insert text 'test' in 'i_login' [name] ... passed in 0.072s
    When insert text '123456' in 'i_code' [name] ... passed in 0.077s
    When submit element on 'btn_submit' ... passed in 0.372s
    When show hint error text 'Пользователь не найден.' in 'error_text_hint' ... passed in 0.035s
    Then close browser ... passed in 0.055s

