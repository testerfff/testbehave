# Created by visakov at 19.03.18


Feature: Forgot Password

    Scenario: Forgot Password "Success"
        Given website open 'forgot_password'

        When click element on 'btn_modal'

      # Проверяем наименование страницы
        When check title 'Восстановление пароля' page

      # Email
        When insert text 'test@test.com' in 'i_email_forgot' [name]

      # Кликаем по кнопке "Восстановить"
        When submit element on 'btn_submit' [name]

        Then close browser


    Scenario: Forgot Password "Email not found"
        Given website open 'forgot_password'

        When click element on 'btn_modal'

      # Email
        When insert text 'test@test.com' in 'i_email_forgot' [name]

      # Кликаем по кнопке "Восстановить"
        When submit element on 'btn_submit' [name]

      # Проверяем текст с ошибкой
        When show hint error text 'Логин или E-mail не найдены.' in 'error_text_hint'

        Then close browser


    Scenario: Forgot Password "Null input"
        Given website open 'forgot_password'

        When click element on 'btn_modal'

      # Email
        When insert text ' ' in 'i_email_forgot' [name]

      # Кликаем по кнопке "Восстановить"
        When submit element on 'btn_submit' [name]

      # Проверяем текст с ошибкой
        When show hint error text 'Логин или E-mail не найдены.' in 'error_text_hint'

        Then close browser
