# Created by visakov at 05.03.18

# -*- coding: utf-8 -*-


import time

import sys
from behave import *
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.common.action_chains import ActionChains

from mapping import site_mapping


@given("website open '{page}'")
def step_impl(context, page):
    context.browser = webdriver.Chrome()

    global current_page
    current_page = site_mapping[page]

    context.browser.get(current_page['url'])

    time.sleep(3)


@when("check title '{name}' page")
def step(context, name):
    global current_page

    name_page = context.browser.title

    assert name_page.encode('utf8') == name.encode('utf8')


@when("insert text '{text}' in '{path}' [xpath]")
def step(context, text, path):
    global current_page

    WebDriverWait(context.browser, 60).until(
        EC.element_to_be_clickable((By.XPATH, current_page[path]))
    )

    context.browser.find_element_by_xpath(current_page[path]).send_keys(text)


@when("insert text '{text}' in '{path}' [name]")
def step(context, text, path):
    global current_page

    WebDriverWait(context.browser, 60).until(
        EC.element_to_be_clickable((By.NAME, current_page[path]))
    )

    context.browser.find_element_by_name(current_page[path]).send_keys(text)


@when("click element on '{path}'")
def step(context, path):
    global current_page

    context.browser.find_element_by_xpath(current_page[path]).click()


@when("click element on '{path}' [name]")
def step(context, path):
    global current_page

    context.browser.find_element_by_name(current_page[path]).click()


@when("submit element on '{path}'")
def step(context, path):
    global current_page

    context.browser.find_element_by_xpath(current_page[path]).submit()


@when("submit element on '{path}' [name]")
def step(context, path):
    global current_page

    context.browser.find_element_by_name(current_page[path]).submit()


@when("click list elements on '{path}' in element '{element}'")
def step(context, path, element):
    global current_page

    lst = context.browser.find_elements_by_xpath(current_page[path])
    lst[int(element)].click()


@when("send name 2 buttons in '{path}' [down and enter]")
def step(context, path):
    global current_page

    WebDriverWait(context.browser, 60).until(
        EC.element_to_be_clickable((By.XPATH, current_page[path]))
    )

    context.browser.find_element_by_xpath(current_page[path]).send_keys(Keys.DOWN + Keys.ENTER)


@when("send name '{text}' 3 buttons in '{path}' [tab and tab and enter]")
def step(context, text, path):
    global current_page

    WebDriverWait(context.browser, 60).until(
        EC.element_to_be_clickable((By.XPATH, current_page[path]))
    )

    context.browser.find_element_by_xpath(current_page[path]).send_keys(text + Keys.TAB + Keys.TAB + Keys.ENTER)


@when("scroll to '{element}'")
def step(context, element):
    global current_page

    element = context.browser.find_element_by_xpath(current_page[element])

    actions = ActionChains(context.browser)
    actions.move_to_element(element).perform()


@when("scroll to '{element}' [name]")
def step(context, element):
    global current_page

    element = context.browser.find_element_by_name(current_page[element])

    actions = ActionChains(context.browser)
    actions.move_to_element(element).perform()


@when("scroll up")
def step(context):
    global current_page

    context.browser.execute_script("window.scrollTo(document.body.scrollHeight, 0);")


@when("scroll down")
def step(context):
    global current_page

    context.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")


@then("the current page is not '{page}'")
def step(context, page):
    assert context.browser.current_url != current_page['url']


@when("show hint error text '{text}' in '{path}'")
def step(context, text, path):
    global current_page

    element = context.browser.find_element_by_xpath(current_page[path])

    if element.text.encode('utf8') != text.encode('utf8'):
        print('error')

        print(element.text)
        print(text)

    assert element.text.encode('utf8') == text.encode('utf8')


@when("show hint error text in '{path}'")
def step(context, path):
    global current_page

    check_element = ['',
                     'Поле "Логин (мин. 3 символа)" обязательно для заполнения',
                     'Поле "Пароль" обязательно для заполнения',
                     'Поле "Подтверждение пароля" обязательно для заполнения',
                     'Поле "Адрес E-mail" обязательно для заполнения',
                     'Поле "Имя" обязательно для заполнения',
                     'Поле "Телефон" обязательно для заполнения']

    elements = context.browser.find_elements_by_xpath(current_page[path])

    el = []

    for e in elements:
        if sys.version_info < (3, 6, 0):
            el.append(e.text.encode('utf8'))
        else:
            el.append(e.text)

    if el != check_element:
        print('error')

        print(el)
        print(check_element)

    assert el == check_element
