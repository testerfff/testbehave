# Created by visakov at 28.02.18

# -*- coding: utf-8 -*-


import time
from behave import *
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from mapping import site_mapping

current_page = []


@given("website '{page}'")
def step_impl(context, page):
    context.browser = webdriver.Chrome()

    global current_page
    current_page = site_mapping[page]

    context.browser.get(current_page['url'])

    time.sleep(3)


@when("insert text '{text}' in '{path}'")
def step(context, text, path):
    global current_page

    WebDriverWait(context.browser, 60).until(
        EC.element_to_be_clickable((By.XPATH, current_page[path]))
    )

    context.browser.find_element_by_xpath(current_page[path]).send_keys(text)


@when("click button '{path}'")
def step(context, path):
    global current_page

    WebDriverWait(context.browser, 60).until(
        EC.element_to_be_clickable((By.XPATH, current_page[path]))
    )

    print(current_page[path])

    context.browser.find_element_by_xpath(current_page[path]).click()


@then("see '{text}' in '{path}'")
def step(context, text, path):
    global current_page

    WebDriverWait(context.browser, 60).until(
        EC.element_to_be_clickable((By.XPATH, current_page[path]))
    )

    elem = context.browser.find_element_by_xpath(current_page[path])

    assert elem.text == text


@when("time '{sleep}'")
def step(context, sleep):
    global current_page

    time.sleep(float(sleep))


@then("current page in '{path}'")
def step(context, path):
    global current_page

    WebDriverWait(context.browser, 60).until(
        EC.element_to_be_clickable((By.XPATH, current_page['profile']))
    )

    assert context.browser.current_url == current_page[path]


@then("close browser")
def step(context):
    context.browser.close()
