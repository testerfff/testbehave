# Created by visakov at 05.03.18


Feature: Registration

    Scenario: Registration Full "Success"
        Given website open 'registration'

        When click element on 'btn_modal'

      # Логин и пароль
        When insert text 'Matthew 1' in 'i_login' [xpath]
        When insert text '123456' in 'i_pass' [xpath]
        When insert text '123456' in 'i_pass_conf' [xpath]

      # Поле email
        When insert text 'test@test.ru' in 'i_email' [xpath]

      # Поля ФИО
        When insert text 'Константин' in 'i_name' [xpath]
        When insert text 'Константинопольский' in 'i_last_name' [xpath]
        When insert text 'Иванович' in 'i_second_name' [xpath]

        When scroll to 'i_phone'

      # Выбор "Пол"
        When click list elements on 'lst_sex' in element '0'
        When send name 2 buttons in 'lst_sex_open' [down and enter]

      # Дата рождения
        When insert text '12.03.2000' in 'i_birthday' [xpath]

      # Номер телефона
        When send name '89022365656' 3 buttons in 'i_phone' [tab and tab and enter]

      # Выбор "Семейное положение"
        When send name 2 buttons in 'lst_marital_status' [down and enter]

      # Количество детей
        When insert text '1' in 'i_count_child' [name]

      # Место рождения
        When insert text 'г. Москва' in 'i_birth_place' [name]

      # Паспортные данные
        When insert text '6500' in 'i_passport_series' [name]
        When insert text '230650' in 'i_passport_number' [name]
        When insert text 'ГУВД г. Москва' in 'i_passport_issuedby' [name]
        When insert text '20.01.2000' in 'i_passport_date' [name]
        When insert text '100-500' in 'i_passport_dep_code' [name]

      # Адрес регистрации
        When insert text '99' in 'i_reg_address_reg' [name]
        When insert text '230001' in 'i_reg_address_index' [name]
        When insert text 'г. Санкт-Петербург' in 'i_reg_address_city' [name]
        When insert text 'ул. Народного Фронта' in 'i_reg_address_street' [name]
        When insert text '100' in 'i_reg_address_house' [name]
        When insert text '7' in 'i_reg_address_build' [name]
        When insert text '3' in 'i_reg_address_bulk' [name]
        When insert text '500' in 'i_reg_address_apart' [name]

      # Адрес фактического проживания
        When insert text '78' in 'i_fact_address_region' [name]
        When insert text '100566' in 'i_fact_address_index' [name]
        When insert text 'г. Пышкин' in 'i_fact_address_city' [name]
        When insert text 'ул. Гвардейская' in 'i_fact_address_street' [name]
        When insert text '99' in 'i_fact_address_house' [name]
        When insert text ' ' in 'i_fact_address_build' [name]
        When insert text '1' in 'i_fact_address_bulk' [name]
        When insert text '18' in 'i_fact_address_apart' [name]

      # Скрол к элементу на странице
        When scroll to 'btn_submit' [name]

      # Кликаем по кнопке "Регистрация"
        When submit element on 'btn_submit' [name]

      # Проверяем страницу на которой находимся
        Then the current page is not 'registration'

        Then close browser


    Scenario: Registration Only Reg "Success"
        Given website open 'registration'

        When click element on 'btn_modal'

      # Логин и пароль
        When insert text 'Matthew' in 'i_login' [xpath]
        When insert text '123456' in 'i_pass' [xpath]
        When insert text '123456' in 'i_pass_conf' [xpath]

      # Поле email
        When insert text 'test@test.ru' in 'i_email' [xpath]

      # Поля ФИО
        When insert text 'Константин' in 'i_name' [xpath]
        When insert text 'Константинопольский' in 'i_last_name' [xpath]
        When insert text 'Иванович' in 'i_second_name' [xpath]

        When scroll to 'i_phone'

      # Выбор "Пол"
        When click list elements on 'lst_sex' in element '0'
        When send name 2 buttons in 'lst_sex_open' [down and enter]

      # Дата рождения
        When insert text '12.03.2000' in 'i_birthday' [xpath]

      # Номер телефона
        When send name '89022365656' 3 buttons in 'i_phone' [tab and tab and enter]

      # Выбор "Семейное положение"
        When send name 2 buttons in 'lst_marital_status' [down and enter]

      # Количество детей
        When insert text '1' in 'i_count_child' [name]

      # Место рождения
        When insert text 'г. Москва' in 'i_birth_place' [name]

      # Паспортные данные
        When insert text '6500' in 'i_passport_series' [name]
        When insert text '230650' in 'i_passport_number' [name]
        When insert text 'ГУВД г. Москва' in 'i_passport_issuedby' [name]
        When insert text '20.01.2000' in 'i_passport_date' [name]
        When insert text '100-500' in 'i_passport_dep_code' [name]

      # Адрес регистрации
        When insert text '99' in 'i_reg_address_reg' [name]
        When insert text '230001' in 'i_reg_address_index' [name]
        When insert text 'г. Санкт-Петербург' in 'i_reg_address_city' [name]
        When insert text 'ул. Народного Фронта' in 'i_reg_address_street' [name]
        When insert text '100' in 'i_reg_address_house' [name]
        When insert text '7' in 'i_reg_address_build' [name]
        When insert text '3' in 'i_reg_address_bulk' [name]
        When insert text '500' in 'i_reg_address_apart' [name]

      # Скрол к элементу на странице
        When scroll to 'chk_address_equal'

      # Адрес регистрации совпадает с фактическим адресом
        When click element on 'chk_address_equal'

      # Скрол к элементу на странице
        When scroll to 'btn_submit' [name]

      # Кликаем по кнопке "Регистрация"
        When submit element on 'btn_submit' [name]

      # Проверяем страницу на которой находимся
        Then the current page is not 'registration'

        Then close browser


    Scenario: Registration "Fail"
        Given website open 'registration'

        When click element on 'btn_modal'

      # Скрол вниз страницы
        When scroll down

      # Кликаем по кнопке "Регистрация"
        When click element on 'btn_submit' [name]

      # Скрол вверх страницы
        When scroll up

        When show hint error text in 'error_text_hint'

        Then close browser