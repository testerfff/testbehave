# Created by visakov at 28.02.18


Feature: Authorization

    Scenario: Authorization Success
        Given website 'authorization'
        When insert text 'testerfff_sip' in 'username'
        When insert text '123456' in 'password'
        When click button 'login button'
        Then current page in 'url cabinet'
        Then close browser


    Scenario: Authorization Fail
        Given website 'authorization'
        When insert text '12345' in 'username'
        When insert text '12345' in 'password'
        When click button 'login button'
        Then see 'Не верно указан логин или пароль' in 'status message'
        Then close browser
