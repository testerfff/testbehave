# Created by visakov at 28.02.18

# -*- coding: UTF-8 -*-

"""
before_step(context, step), after_step(context, step)
    These run before and after every step.
    The step passed in is an instance of Step.
before_scenario(context, scenario), after_scenario(context, scenario)
    These run before and after each scenario is run.
    The scenario passed in is an instance of Scenario.
before_feature(context, feature), after_feature(context, feature)
    These run before and after each feature file is exercised.
    The feature passed in is an instance of Feature.
before_tag(context, tag), after_tag(context, tag)
"""

import os
import errno
from lxml import etree
import datetime
from json import load


def before_all(context):
    print('before_all')

    context.config.setup_logging()

    context.settings = load(open('features/conf.json'))
    context.base_url = ""
    context.login = context.settings['login']

    # print(context.settings)
    # print(context.login)

    sep = os.sep
    current_dir = os.path.dirname(os.path.abspath(os.path.join(__file__, os.pardir))) + sep + 'reports'
    print(current_dir)

    for file in os.listdir(current_dir):
        if file.endswith(".xml"):
            os.remove(current_dir + sep + file)


def after_all(context):
    print('after_all')

    sep = os.sep
    date_now = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")

    current_dir = os.path.dirname(os.path.abspath(os.path.join(__file__, os.pardir))) + sep + 'reports'
    sub_folders = [x[0] for x in os.walk(current_dir)]

    for k in sub_folders:

        if 'html' not in k:
            try:
                os.makedirs(current_dir + sep + 'html')
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise

        if 'html' + sep + date_now not in k:
            try:
                os.makedirs(current_dir + sep + 'html' + sep + date_now)
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise

    xslt_doc = etree.parse("reports" + sep + "templates" + sep + "default.xslt")
    xslt_transformer = etree.XSLT(xslt_doc)

    for file in os.listdir(current_dir):
        if file.endswith(".xml"):
            print(file)

            path_xml = "reports" + sep + "{}".format(file)

            source_doc = etree.parse(path_xml)
            output_doc = xslt_transformer(source_doc)

            output_doc.write("reports" + sep + "html" + sep + "{}".format(date_now) + sep + "{}.html".format(
                file.split('.')[0]), pretty_print=True)
