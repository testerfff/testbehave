# Created by visakov at 20.03.18


Feature: Change Password

    Scenario: Change Password "Success"
        Given website open 'change_password'

        When click element on 'btn_modal'

      # Проверяем наименование страницы
        When check title 'Изменение пароля' page

      # Заполнение полей формы
        When insert text 'test' in 'i_login' [name]
        When insert text 'testtest' in 'i_check_string' [name]
        When insert text '123456' in 'i_new_password' [name]
        When insert text '123456' in 'i_new_password_confirm' [name]

      # Кликаем по кнопке "Изменить пароль"
        When submit element on 'btn_submit'

        Then close browser


    Scenario: Change Password "Empty inputs"
        Given website open 'change_password'

        When click element on 'btn_modal'

      # Кликаем по кнопке "Изменить пароль"
        When submit element on 'btn_submit'

      # Проверяем текст с ошибкой
        When show hint error text 'Логин должен быть не менее 3 символов.' in 'error_text_hint_login'

        Then close browser


    Scenario: Change Password "Short Password"
        Given website open 'change_password'

        When click element on 'btn_modal'

      # Заполнение полей формы
        When insert text 'test' in 'i_login' [name]
        When insert text 'testtest' in 'i_check_string' [name]
        When insert text '123' in 'i_new_password' [name]
        When insert text '123' in 'i_new_password_confirm' [name]

      # Кликаем по кнопке "Изменить пароль"
        When submit element on 'btn_submit'

      # Проверяем текст с ошибкой
        When show hint error text 'Логин должен быть не менее 3 символов.' in 'error_text_hint_password'

        Then close browser


    Scenario: Change Password "Passwords do not match"
        Given website open 'change_password'

        When click element on 'btn_modal'

      # Заполнение полей формы
        When insert text 'test' in 'i_login' [name]
        When insert text 'testtest' in 'i_check_string' [name]
        When insert text '123456' in 'i_new_password' [name]
        When insert text '123123' in 'i_new_password_confirm' [name]

      # Кликаем по кнопке "Изменить пароль"
        When submit element on 'btn_submit'

      # Проверяем текст с ошибкой
        When show hint error text 'Неверное подтверждение пароля.' in 'error_text_hint_password_not_match'

        Then close browser