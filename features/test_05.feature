# Created by visakov at 21.03.18


Feature: Confirmation Email

    Scenario: Confirmation Email "Success"
        Given website open 'confirm_email'

        When click element on 'btn_modal'

      # Проверяем наименование страницы
        When check title 'Регистрация' page

      # Заполнение полей формы
        When insert text 'test' in 'i_login' [name]
        When insert text '123456' in 'i_code' [name]

      # Кликаем по кнопке "Подтвердить"
        When submit element on 'btn_submit'

        Then close browser


    Scenario: Confirmation Email "Empty inputs"
        Given website open 'confirm_email'

        When click element on 'btn_modal'

      # Кликаем по кнопке "Подтвердить"
        When submit element on 'btn_submit'

      # Проверяем текст с ошибкой
        When show hint error text 'Пользователь не найден.' in 'error_text_hint'

        Then close browser


    Scenario: Confirmation Email "Not a valid user"
        Given website open 'confirm_email'

        When click element on 'btn_modal'

      # Заполнение полей формы
        When insert text 'test' in 'i_login' [name]
        When insert text '123456' in 'i_code' [name]

      # Кликаем по кнопке "Подтвердить"
        When submit element on 'btn_submit'

      # Проверяем текст с ошибкой
        When show hint error text 'Пользователь не найден.' in 'error_text_hint'

        Then close browser